package devTo;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DevToStepDefinitions {

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "chromedriver/chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
    }

    @Given("I am on the devTo main page")
    public void i_am_on_the_dev_to_main_page() {
        driver.get("https://dev.to");
    }

    @When("I click on week button")
    public void i_click_on_week_button() {
        driver.findElement(By.xpath("//a[@href='/top/week']")).click();
    }
    @When("I click on month button")
    public void i_click_on_month_button() {
        driver.findElement(By.xpath("//a[@href='/top/month']")).click();
    }
/*    @Then("I should be redirected to week page")
    public void i_should_be_redirected_to_week_page() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlToBe("https://dev.to/top/week"));
        String title = driver.getTitle();
        Assert.assertTrue(title.contains("Top posts this week"));
    }*/ // po napisanuniu następnego @Then jest teraz nie potrzebny

    @Then("I should be redirected to {string} page")
    public void i_should_be_redirected_to_page(String expected) {
        wait.until(ExpectedConditions.urlContains(expected)); // poczekaj dopóki url będzie zawierał expected czyli week
        String title = driver.getTitle();
        Assert.assertTrue(title.contains(expected));
    }


  /*  @Then("I should be redirected to month page")
    public void i_should_be_redirected_to_month_page() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.urlToBe("https://dev.to/top/month"));
        String title = driver.getTitle();
        Assert.assertTrue(title.contains("Top posts this month"));
    }*/
    @Given("I am on the devTo main page")
    public void i_am_on_the_dev_to_main_page() {
        driver.get("https://dev.to");
    }

    @When("I go to podcasts page")
    public void i_go_to_podcasts_page() {
        driver.findElement(By.xpath("//*[@id="sidebar-nav"]/div/a[2]")).click();
        //a[@href='/top/week']"

}
